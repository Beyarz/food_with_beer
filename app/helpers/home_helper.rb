# frozen_string_literal: true

module HomeHelper
  BEER_ENDPOINT = "https://api.punkapi.com/v2/beers"
end
