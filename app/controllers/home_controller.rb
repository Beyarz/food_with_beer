# frozen_string_literal: true

require "net/https"
require "json"

class HomeController < ApplicationController
  include HomeHelper

  def index
    @page = page_state
    @search_query = params[:query].present? ? params[:query].tr(" ", "_") : nil

    page_response = Rails.cache.fetch("retrieve_beers_page_#{@page}_search_#{@search_query}", expires_in: 12.hours) { retrieve_beers(@page, @search_query) }
    @response_body = JSON.parse(page_response)

    next_page = Rails.cache.fetch("retrieve_beers_page_#{@page + 1}_search_#{@search_query}", expires_in: 12.hours) { retrieve_beers(@page + 1, @search_query) }
    @next_page = JSON.parse(next_page)
  end

  private
    def retrieve_beers(page, search_query)
      # PunkApi cannot handle empty parameters
      # so we fully exclude it in the url if
      # no value is provided
      if not search_query.nil?
        filter_by_query = "&beer_name=#{search_query}"
      end

      assembled_url = "#{BEER_ENDPOINT}/?per_page=10&page=#{page}#{filter_by_query}"

      # Used for debugging only
      # puts "Sent request to: #{assembled_url}"

      uri = URI(assembled_url)
      Net::HTTP.get(uri)
    end

    def page_state
      params[:page].nil? ? 1 : params[:page].to_i
    end
end
