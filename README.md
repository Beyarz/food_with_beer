# Food with beer

A webapp to see which beer suits you the most

## Demo

![Home](/demo/home.png)
![Description](/demo/description.png)

## System requirements

- Ruby 3.0.0
- Rails 6.1
- Bundler 2.2
- Nodejs 16 with NPM 8.1
- Yarn 1.22
- Docker 4.5.0 (optional)

## Environment

### Working environments

- macOS 12.2
- Ubuntu 20.04.4 LTS
- Windows 10 (With WSL2)

### Non-working environments

- Windows 10 (Use the Dockerfile)

## Getting started

```
git clone https://gitlab.com/Beyarz/food_with_beer.git
cd food_with_beer
bundle
rails webpacker:install
```

## Run server

### Development mode

Cache is disabled by default in Rails, so you need to execute the following command once:

`rails dev:cache`

After that you can boot up the server,

`rails server`

The website can then be accessed on [http://localhost:3000/home](http://127.0.0.1:3000/home)

### Production mode

Regenerate secret key once using the following command:

`export SECRET_KEY_BASE=$(rails secret)`


`rails assets:precompile`

```
export RAILS_SERVE_STATIC_FILES=true
export RAILS_ENV=production
```

`rails server -b 127.0.0.1`

After the following steps you should be able to open the same link as above: [http://localhost:3000/home](http://127.0.0.1:3000/home)

### Production mode using Docker

#### Build the image

`docker build -t food_with_beer .`

#### Run the container

`docker run -p 3000:3000 food_with_beer`

The website can then be accessed on [http://localhost:3000/home](http://127.0.0.1:3000/home)

## Development in container

### Windows users

`docker run -v %cd%:/app -w /app --name ruby-dev -p 3000:3000 --rm -it ruby:3.0.0 sh ./entrypoint.sh`

### UNIX users

`docker run -v $(pwd):/app -w /app --name ruby-dev -p 3000:3000 --rm -it ruby:3.0.0 sh ./entrypoint.sh`

### Environment shell

If you'd like to access the shell then you can simply remove the last line `./entrypoint.sh`.

The website can then be accessed on [http://localhost:3000/home](http://127.0.0.1:3000/home)

## Lint / Format

`rubocop -A`

## Testing

## Perform all tests (including system tests)

`rails test:all`

## Run all tests (excluding system tests)

`rails test`

### System tests

`rails test:system`

### Integration tests

`rails test:integration`

### Controllers tests

`rails test:controllers`
