FROM ruby:3.0.0

WORKDIR /food_with_beer
COPY . /food_with_beer

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt update
RUN apt install -y nodejs yarn

RUN bundle
RUN rails yarn:install
RUN rails assets:precompile
RUN rails webpacker:compile
RUN export SECRET_KEY_BASE=$(rails secret)

EXPOSE 3000
ENV SECRET_KEY_BASE=$(SECRET_KEY_BASE)
ENV RAILS_ENV=production
ENV RAILS_SERVE_STATIC_FILES=true
ENTRYPOINT [ "rails" ]
CMD [ "server", "-b", "0.0.0.0" ]
