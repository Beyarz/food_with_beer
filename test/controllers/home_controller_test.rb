# frozen_string_literal: true

require "test_helper"

class HomeControllerTest < ActionDispatch::IntegrationTest
  test "index page should lead to first page" do
    get home_path
    assert_select "p.lead", "Page 1"
    assert_response :success
  end

  test "request with empty query should lead to home page" do
    get home_path(query: nil)
    assert_select "p.lead", "Page 1"
    assert_response :success
  end

  test "root should lead to home page" do
    get root_path
    assert_select "p.lead", "Page 1"
    assert_response :success
  end
end
