# frozen_string_literal: true

require "application_system_test_case"

class PaginationsTest < ApplicationSystemTestCase
  test "should go to the next page when clicking on next" do
    visit home_path
    assert_selector "p", text: "Page 1"
    click_on "Next"
    assert_selector "p", text: "Page 2"
  end

  test "should go to the previous page when clicking on previous" do
    visit home_path page: 2
    assert_selector "p", text: "Page 2"
    click_on "Previous"
    assert_selector "p", text: "Page 1"
  end

  test "should go to home_path when clicking on home" do
    visit home_path page: 2
    assert_selector "p", text: "Page 2"
    click_on "Home"
    assert_selector "p", text: "Page 1"
  end

  test "previous button should be disabled on first page" do
    visit home_path
    assert find(".page-item.disabled")
  end
end
