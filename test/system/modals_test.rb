# frozen_string_literal: true

require "application_system_test_case"

class ModalsTest < ApplicationSystemTestCase
  test "clicking on read more opens the modal with further description" do
    visit home_path
    click_on "Read more", match: :first
    assert_selector "p", text: "Description"
    assert_selector "p", text: "Food pairing"
  end

  test 'clicking on "Sounds good!" should close modal' do
    visit home_path
    click_on "Read more", match: :first
    assert find("div.modal")
    assert click_on "Sounds good!"
  end
end
