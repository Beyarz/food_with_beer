# frozen_string_literal: true

require "test_helper"

class HomeTest < ActionDispatch::IntegrationTest
  test "page number should be displayed" do
    get home_path
    assert_select "p.lead", "Page 1"
  end

  test "page number should change the next page" do
    get home_path(page: 2)
    assert_select "p.lead", "Page 2"
  end
end
