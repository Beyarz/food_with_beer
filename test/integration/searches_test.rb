# frozen_string_literal: true

require "test_helper"

class SearchesTest < ActionDispatch::IntegrationTest
  test 'should see an "not found" message when there is no result' do
    get home_path, params: { query: "this_query_should_return_an_not_found_message" }
    assert_select "p.text-muted", "Nothing found..."
  end

  test 'should see "Sunk Punk" as first result when searching for "Punk"' do
    get home_path, params: { query: "Punk" }
    assert_select "td", "Sunk Punk"
  end
end
